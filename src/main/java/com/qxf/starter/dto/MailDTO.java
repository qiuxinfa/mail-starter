package com.qxf.starter.dto;

import org.springframework.lang.Nullable;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

public class MailDTO {
    /**
     * 接收者
     */
    private String[] to;

    /**
     * 抄送
     */
    @Nullable
    private String[] cc;

    /**
     * 密码抄送
     */
    @Nullable
    private String[] bcc;

    /**
     * 发送日期
     */
    @Nullable
    private Date sentDate;

    /**
     * 发送主题
     */
    @Nullable
    private String subject;

    /**
     * 邮件内容
     */
    @Nullable
    private String text;

    /**
     * 模板路径
     */
    @Nullable
    private String templatePath;

    /**
     * 模板参数
     */
    @Nullable
    private Map<String,Object> templateParams;

    /**
     * 图片cid -> 图片路径的对应关系
     */
    @Nullable
    private Map<String,String> picMap;

    /**
     * 附件路径
     */
    @Nullable
    private Map<String,String> attachmentMap;;

    public String[] getTo() {
        return to;
    }

    public void setTo(String[] to) {
        this.to = to;
    }

    @Nullable
    public String[] getCc() {
        return cc;
    }

    public void setCc(@Nullable String[] cc) {
        this.cc = cc;
    }

    @Nullable
    public String[] getBcc() {
        return bcc;
    }

    public void setBcc(@Nullable String[] bcc) {
        this.bcc = bcc;
    }

    @Nullable
    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(@Nullable Date sentDate) {
        this.sentDate = sentDate;
    }

    @Nullable
    public String getSubject() {
        return subject;
    }

    public void setSubject(@Nullable String subject) {
        this.subject = subject;
    }

    @Nullable
    public String getText() {
        return text;
    }

    public void setText(@Nullable String text) {
        this.text = text;
    }

    @Nullable
    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(@Nullable String templatePath) {
        this.templatePath = templatePath;
    }

    @Nullable
    public Map<String, Object> getTemplateParams() {
        return templateParams;
    }

    public void setTemplateParams(@Nullable Map<String, Object> templateParams) {
        this.templateParams = templateParams;
    }

    @Nullable
    public Map<String, String> getPicMap() {
        return picMap;
    }

    public void setPicMap(@Nullable Map<String, String> picMap) {
        this.picMap = picMap;
    }

    @Nullable
    public Map<String, String> getAttachmentMap() {
        return attachmentMap;
    }

    public void setAttachmentMap(@Nullable Map<String, String> attachmentMap) {
        this.attachmentMap = attachmentMap;
    }

    @Override
    public String toString() {
        return "MailDTO{" +
                "to=" + Arrays.toString(to) +
                ", cc=" + Arrays.toString(cc) +
                ", bcc=" + Arrays.toString(bcc) +
                ", sentDate=" + sentDate +
                ", subject='" + subject + '\'' +
                ", text='" + text + '\'' +
                ", templatePath='" + templatePath + '\'' +
                ", templateParams=" + templateParams +
                ", picMap=" + picMap +
                ", attachmentMap=" + attachmentMap +
                '}';
    }
}
