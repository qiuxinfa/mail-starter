package com.qxf.starter.config;

import com.qxf.starter.properties.MailProperties;
import com.qxf.starter.service.MailService;
import com.qxf.starter.service.impl.MailServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(value = MailProperties.class)
@ConditionalOnProperty(prefix = "mail", value = "enable", havingValue = "true")
public class MailAutoConfig {

    @Bean
    public MailService mailService(){
        return new MailServiceImpl();
    }
}
