package com.qxf.starter.service;


import com.qxf.starter.dto.MailDTO;

public interface MailService {
    /**
     * 发送纯文本内容的邮件
     * @param dto
     * @return 是否发送成功
     */
    boolean sendSimpleEmail(MailDTO dto);

    /**
     * 发送使用freemarker模板制作的邮件
     * @param dto
     * @return 是否发送成功
     */
    boolean sendTemplateEmail(MailDTO dto);
}
