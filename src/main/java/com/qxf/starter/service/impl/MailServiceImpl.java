package com.qxf.starter.service.impl;


import com.qxf.starter.dto.MailDTO;
import com.qxf.starter.properties.MailProperties;
import com.qxf.starter.service.MailService;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

public class MailServiceImpl implements MailService {

    @Value("${spring.mail.username}")
    private String from;

    private JavaMailSenderImpl mailSender;

    @Autowired
    freemarker.template.Configuration configuration;

    @Autowired
    private MailProperties mailProperties;

    @PostConstruct
    private void initMailSender(){
        mailSender = new JavaMailSenderImpl();
        mailSender.setHost(mailProperties.getHost());
        mailSender.setUsername(mailProperties.getUsername());
        mailSender.setPassword(mailProperties.getPassword());
        mailSender.setPort(mailProperties.getPort());
        mailSender.setDefaultEncoding("Utf-8");
        Properties p = new Properties();
        p.setProperty("mail.debug", mailProperties.getDebug());
        p.setProperty("mail.smtp.ssl.enable", mailProperties.getEnableSsl());
        mailSender.setJavaMailProperties(p);
    }

    @Override
    public boolean sendSimpleEmail(MailDTO dto) {
        boolean success;
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(dto.getTo());
        message.setSubject(dto.getSubject());
        message.setText(dto.getText());
        try {
            mailSender.send(message);
            success = true;
            System.out.println("给 "+ Arrays.toString(dto.getTo()) +" 发送邮件成功");
        } catch (MailException e) {
            System.err.println("给 "+ Arrays.toString(dto.getTo()) +" 发送邮件失败");
            success = false;
        }
        return success;
    }

    @Override
    public boolean sendTemplateEmail(MailDTO dto) {
        boolean success;
        try {
            MimeMessage msg = mailSender.createMimeMessage();//创建模拟的消息
            MimeMessageHelper helper = new MimeMessageHelper(msg, true);//创建模拟的消息工具
            helper.setFrom(from);//发件人
            helper.setTo(dto.getTo());
            helper.setSubject(dto.getSubject());//发送主题
            Template template = configuration.getTemplate(dto.getTemplatePath());
            StringWriter writer = new StringWriter();
            template.process(dto.getTemplateParams(), writer);
            //内容是否设置成html,true代表是
            helper.setText(writer.toString(), true);

            // 判断邮件内容是否嵌套了图片
            if (!CollectionUtils.isEmpty(dto.getPicMap())){
                for (Map.Entry<String, String> entry : dto.getPicMap().entrySet()) {
                    helper.addInline(entry.getKey(),new FileSystemResource(new File(entry.getValue())));
                }
            }

            // 判断是否存在附件
            if (!CollectionUtils.isEmpty(dto.getAttachmentMap())){
                for (Map.Entry<String, String> entry : dto.getAttachmentMap().entrySet()) {
                    helper.addAttachment(entry.getKey(),new File(entry.getValue()));
                }
            }

            mailSender.send(msg);
            success = true;
            System.out.println("给 "+ Arrays.toString(dto.getTo()) +" 发送邮件成功");
        } catch (Exception e) {
            System.err.println("给 "+ Arrays.toString(dto.getTo()) +" 发送邮件失败");
            success = false;
        }
        return success;
    }
}
